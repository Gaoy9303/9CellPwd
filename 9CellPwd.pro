#-------------------------------------------------
#
# Project created by QtCreator 2019-10-17T09:24:07
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = 9CellPwd
TEMPLATE = app


SOURCES += main.cpp \
    c9_cellpwd_dlg.cpp

HEADERS  += \
    c9_cellpwd_dlg.h

FORMS    +=
