#include "c9_cellpwd_dlg.h"

c9_CellPwd_Dlg::c9_CellPwd_Dlg(QWidget *parent) :
    QDialog(parent)
{
    this->setWindowFlags(Qt::FramelessWindowHint);
    c9_resize(340);
    this->setStyleSheet("background-color:rgb(255,255,255)");
}

void c9_CellPwd_Dlg::c9_resize(int w){

    bigCube_wh=w;
    bigCube_border=20;
    int cube_wh=(bigCube_wh-2*bigCube_border)/4;

    PRTLOG()<<bigCube_wh<<"x"<<bigCube_wh;
    PRTLOG()<<"cube_wh:"<<cube_wh;
    for(int x=0;x<3;x++){
        for(int y=0;y<3;y++){
            QPoint cpoint =  QPoint((bigCube_border+((3*y+1)*cube_wh)/2),(bigCube_border+((3*x+1)*cube_wh)/2));
            c9_cellbtn[x*3+y].centerPoint = cpoint;
            c9_cellbtn[x*3+y].outRound_Rect= QRect((cpoint.x()-cube_wh/2),(cpoint.y()-cube_wh/2),cube_wh,cube_wh);
            c9_cellbtn[x*3+y].outRound_R = cube_wh/2;
            c9_cellbtn[x*3+y].inRound_Rect= QRect((cpoint.x()-cube_wh/4),(cpoint.y()-cube_wh/4),cube_wh/2,cube_wh/2);
            c9_cellbtn[x*3+y].inRound_R = cube_wh/4;
        }
    }
    this->resize(bigCube_wh,bigCube_wh);
    this->update();
}

void c9_CellPwd_Dlg::paintEvent(QPaintEvent * event){
    QPainter p;
    p.begin(this);
    p.setRenderHint(QPainter::Antialiasing, true);
    for(int i=0;i<9;i++){
        p.setPen(Qt::white);
        p.setBrush(QBrush(Qt::white,Qt::NoBrush));
        p.drawEllipse(c9_cellbtn[i].centerPoint,c9_cellbtn[i].outRound_R,c9_cellbtn[i].outRound_R);
        p.setPen(Qt::lightGray);
        p.setBrush(QBrush(Qt::lightGray,Qt::SolidPattern));
        p.drawEllipse(c9_cellbtn[i].centerPoint,c9_cellbtn[i].inRound_R,c9_cellbtn[i].inRound_R);

        for(int ck=0;ck<pwdPathList.count();ck++){
            if(pwdPathList.at(ck)==i){
                p.setPen(Qt::red);
                p.setBrush(QBrush(Qt::white,Qt::NoBrush));
                p.drawEllipse(c9_cellbtn[i].centerPoint,c9_cellbtn[i].outRound_R,c9_cellbtn[i].outRound_R);
                p.setPen(Qt::red);
                p.setBrush(QBrush(Qt::red,Qt::SolidPattern));
                p.drawEllipse(c9_cellbtn[i].centerPoint,c9_cellbtn[i].inRound_R,c9_cellbtn[i].inRound_R);
            }
        }
    }
    p.end();
    if(c9_mousePressed){
        p.begin(this);
        for(int i=0;i<pwdPathList.count()-1;i++){
            p.setPen(Qt::red);
            p.drawLine(c9_cellbtn[pwdPathList.at(i)].centerPoint,c9_cellbtn[pwdPathList.at(i+1)].centerPoint);
        }
        p.end();
    }
}
c9_CellPwd_Dlg::~c9_CellPwd_Dlg()
{

}
void c9_CellPwd_Dlg::mousePressEvent(QMouseEvent *event){
    c9_mousePressed=true;
    pwdPathList.clear();
    for(int i=0;i<9;i++){
        int xx=abs(event->pos().x() -c9_cellbtn[i].centerPoint.x());
        int yy=abs(event->pos().y() -c9_cellbtn[i].centerPoint.y());
        int l = sqrt(xx*xx+yy*yy);
        if(l <= (c9_cellbtn[i].outRound_R*3/2))
            cu_Pos=i;
    }
    prev_Pos = cu_Pos;
    pwdPathList.append(cu_Pos);
}

void c9_CellPwd_Dlg::mouseReleaseEvent(QMouseEvent *event){
    c9_mousePressed=false;
}
void c9_CellPwd_Dlg::mouseMoveEvent(QMouseEvent * event){
        if(c9_mousePressed){
            QPoint sp = event->pos();
            for(int i=0;i<9;i++){
                int xx=abs(sp.x() -c9_cellbtn[i].centerPoint.x());
                int yy=abs(sp.y() -c9_cellbtn[i].centerPoint.y());
                int l = sqrt(xx*xx+yy*yy);
                if(l <= (c9_cellbtn[i].outRound_R*3/2)){
                    if(i != cu_Pos){
                        if(i != prev_Pos){
                            prev_Pos = cu_Pos;
                            cu_Pos =i;
                            if(!pwdPathList.contains(cu_Pos))
                                   pwdPathList.append(cu_Pos);
                        }else{
                            pwdPathList.removeLast();
                            cu_Pos=prev_Pos;
                        }

                    }
                }
            }
            this->update();
        }
}

void c9_CellPwd_Dlg::on_P3_tBt_clicked()
{
    c9_mousePressed=true;
}
