#ifndef C9_CELLPWD_DLG_H
#define C9_CELLPWD_DLG_H

/*9 cell pwd  num  sort
 * 0  1  2
 * 3  4  5
 * 6  7  8
*/
#include <QDialog>
#include <QToolButton>
#include <QPainter>
#include <QMouseEvent>
#include <QBrush>
#include <QDebug>
#include <QPaintEvent>
#include <QPainter>
#include <QRect>
#include <QList>
#include "math.h"
#define PRTLOG()    qDebug()<<__FILE__<<__FUNCTION__<<"Line:"<<__LINE__<<":"



typedef struct _cell{
    QPoint centerPoint;
    QRect  outRound_Rect;
    int    outRound_R;
    QRect  inRound_Rect;
    int    inRound_R;
}cellBtn;
class c9_CellPwd_Dlg : public QDialog
{
    Q_OBJECT

public:
    explicit c9_CellPwd_Dlg(QWidget *parent = 0);
    ~c9_CellPwd_Dlg();

    void c9_resize(int w);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent * event);

    void paintEvent(QPaintEvent * event);
private slots:
    void on_P3_tBt_clicked();

private:
    bool c9_mousePressed=false;

    int bigCube_wh;
    int bigCube_border;

    cellBtn c9_cellbtn[9];
    QList <int> pwdPathList;
    int prev_Pos,cu_Pos;
};

#endif // P_9CELLPWD_DLG_H
